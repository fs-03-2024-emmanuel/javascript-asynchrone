/*const axios = require('axios').default;

function consomation(){
    const url = "https://jsonplaceholder.typicode.com/todos";
    let responses = null;
    fetch(url)
        .then(({type, url, body}) => {

            console.log(`Le type de ma 
            reponse est: ${type} et l'url est: ${url}`)
            console.log(body)
        })
        .catch(error => console.log(error))
}

async function consomationAsyn(){

    try {
        const urls = "https://jsonplaceholder.typicode.com/todos";
        let {type, url, body} = await axios.get(urls)
        console.log(`Le type de ma 
                reponse est: ${type} et l'url est: ${url}`)
        console.log(body)
    } catch (error) {
        console.log("error")
    }
}

consomationAsyn()  */

// une fonction qui prend un tableau d'objet de personne en argument et qui retourne le nom de la personne Toto
// Si il y'a pas de Toto on retourne une erreur pas de toto

/*function toto (users){
    
    const promise = new Promise((resolve, reject)=>{
        const user = users.find(value => value.name == "toto");
        if(user != null){
            resolve(user.name)
        }
        reject("pas de tot")
    })

    return promise;
}

const tabs = [{name:"tata"}, {name: "titi"}, {name:"totos"}];
toto(tabs)
    .then(res=>{
        return res
    })
    .then(res=>console.log(res))
    .catch(err=>console.log(err))*/


// Ecrire une fonction qui demande un a un utilisateur son nom et son prenom ainsi que son age. si l'age est inferieur a 10 on renvoi toutes les information sinon le message d'erreur vous n'etes pas autoriser sur cette plateforme

/*let nom1 = prompt('entrez votre nom')
let prenom1 = prompt('entrez votre premon')
let age1 = prompt('entrez votre age')
let age2 = Number(age1)

let personne = {name : nom1, surname : prenom1, age : age2}

function getAge (pers){
    
   let promesse = new Promise ((resolve, reject) => {
    
    ( pers.age < 10) ? 
    resolve(`le nom de l'utilisateur est ${pers.name}, ${pers.surname} et il a ${pers.age} ans`) 
    : reject('Vous n etes pas autoriser sur cette plateforme')
}
)
    return promesse
}


getAge(personne).then((oui) => {console.log(oui)}).catch((non) => console.log(non)) */


// creer une fonction qui prend en argument un tableau de chaines 
// et qui retourne le contenu du tableau si le tableau contient la chaine 'mangue' 
// sinon retourner le message 'Votre tableau est incomplet' 

function verifMango(fruit) {

    let promesse = new Promise ((resolve,reject) => {
        fruit.includes('mangue') ? resolve(fruit) : reject(`Votre tableau est incomplet`)
    })
    return promesse
}

let aaa = ['orange', 'banane']
verifMango(aaa).then((oui) => console.log(oui)).catch((non) => console.log(non))

